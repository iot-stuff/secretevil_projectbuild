#include <ESP8266WiFi.h>
#include <espnow.h>
#include <ESPAsyncWebServer.h>
#include "ESP_Webpage.h"
#include <ArduinoJson.h>

#define CHAN_AP 1

typedef struct esp_now_peer_info {
    u8 peer_addr[6];
    uint8_t channel;
    uint8_t encrypt;  
}esp_now_peer_info_t;

struct ESPNow_Msg {
    char mac[20];
    uint8_t io;
    uint8_t value;
};

AsyncWebServer server(80);
char hardwareMac[20];

uint8_t kok[16]= {0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66};
uint8_t key[16]= {0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44, 0x33, 0x44};
uint8_t controller_mac[6] = {0xF4, 0xCF, 0xA2, 0xD7, 0xA8, 0xB8};
uint8_t slave_mac[6] = {0xF4, 0xCF, 0xA2, 0xD7, 0xCA, 0x81};
uint8_t invalidMac[6] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

bool processCommand(ESPNow_Msg receiv_msg) {
    if (strcmp(receiv_msg.mac, hardwareMac) == 0 || strcmp(receiv_msg.mac, (char*)invalidMac) == 0) {
        Serial.println("\nDOREMON");
        // Serial.println(String(receiv_msg.io));
        // Serial.println(String(value));

        uint8_t value = (receiv_msg.value < 2) ? receiv_msg.value : (!digitalRead(receiv_msg.io));
        digitalWrite(receiv_msg.io, value);
        return true;
    }

    return false;
}

void OnDataSent(uint8_t *mac_addr, uint8_t status) {
    Serial.println("Send data");
}

void OnDataRecv(uint8_t *mac_addr, uint8_t *data, uint8_t len) { 
    Serial.println("\nReceive Data");
    // digitalWrite(2, !digitalRead(2));
    ESPNow_Msg receiv_msg;
    memcpy(&receiv_msg, data, sizeof(receiv_msg));

    Serial.print(F("mac: ")); Serial.print(String(receiv_msg.mac));
    Serial.print(F(", io: ")); Serial.print(String(receiv_msg.io));
    Serial.print(F(", value: ")); Serial.print(String(receiv_msg.value));
    processCommand(receiv_msg);
}

void startServer() {
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send_P(200, "text/html", webpage, NULL);
    }); 

    server.on("/test", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send_P(200, "text/plain", "OK");
        Serial.println("received test request");
    });

    server.on("/ioCommand", HTTP_GET, [](AsyncWebServerRequest *request) {

    }, NULL, [](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total) {
        Serial.println("received ioCommand");
        DynamicJsonDocument doc(1024);
	    deserializeJson(doc, data);
        String macStr = doc["mac"];

        ESPNow_Msg message; 
        macStr.toCharArray(message.mac, sizeof(message.mac));
        message.io = doc["io"].as<uint8_t>();
        message.value = doc["dValue"].as<uint8_t>();
        Serial.println("mac: " + macStr + ", io: " + String(message.io) + ", dValue: " + String(message.value));

        if (!processCommand(message)) {
            int result = esp_now_send(slave_mac, (uint8_t*)&message, sizeof(message));
            if (result == 0) {
                Serial.println(F("Sent with success"));
            }
        }

        request->send_P(200, "text/plain", "OK");
    });
    
    server.begin();
}

bool _isMaster = true;

void startESPNow(bool isMaster) {
    // _isMaster = isMaster;
    pinMode(2, OUTPUT);
    pinMode(12, OUTPUT);
    
    WiFi.macAddress().toCharArray(hardwareMac, sizeof(hardwareMac));
    Serial.print(F("\nHardwareMac: ")); Serial.println(String(hardwareMac));

    WiFi.softAPdisconnect(true);
    WiFi.persistent(false);
    // WiFi.disconnect();
    // WiFi.mode(WIFI_OFF);
    ESP.eraseConfig();    
    delay(100);

    if (_isMaster) {
        Serial.println("ESP-NOW Gateway");
        WiFi.mode(WIFI_AP_STA);
        
        // wifi_set_macaddr(STATION_IF, controller_mac);
        WiFi.begin("NETGEAR26", "0001112223");
        Serial.println(F("Connecting to Router Access Point"));
        while (WiFi.status() != WL_CONNECTED) {
            delay(500);
            Serial.print(F("."));
        }

        WiFi.softAP("beehive", "nosuperpower", 1, true, 1);        // (ssid, password, channel, hidden)
        startServer();

    } else {
        Serial.println("ESP Client");
        WiFi.mode(WIFI_STA);
        wifi_set_macaddr(STATION_IF, slave_mac);

        WiFi.begin("beehive", "nosuperpower");
        Serial.println(F("Connecting to ESP Access Point"));
        while (WiFi.status() != WL_CONNECTED) {
            delay(500);
            Serial.print(F("."));
        }
    }

    //Init ESP-NOW
    if (esp_now_init() != 0) {
        Serial.println(F("Error initializing ESP-NOW"));
        return;
    }

    Serial.println(F("ESP-NOW STARTED"));
    
    if (_isMaster) {
        esp_now_set_self_role(ESP_NOW_ROLE_CONTROLLER);
    //    esp_now_add_peer(slave_mac1, ESP_NOW_ROLE_SLAVE, 1, NULL, 0);
        esp_now_add_peer(slave_mac, ESP_NOW_ROLE_SLAVE, 1, key, 16);
        esp_now_set_peer_key(slave_mac, key, 16);
    } else {
        esp_now_set_self_role(ESP_NOW_ROLE_SLAVE);
    //    esp_now_add_peer(controller_mac, ESP_NOW_ROLE_CONTROLLER, 1, NULL, 0);
        esp_now_add_peer(controller_mac, ESP_NOW_ROLE_CONTROLLER, 1, key, 16);
        esp_now_set_peer_key(controller_mac, key, 16);
    }

    esp_now_register_recv_cb(OnDataRecv);
    esp_now_register_send_cb(OnDataSent);
    esp_now_set_kok(kok, 16);
}

bool toggle = false;

void runESPNow() {
    if (!_isMaster) { return; }
    toggle = !toggle; 

    ESPNow_Msg message; 
    memcpy(message.mac, invalidMac, sizeof(message.mac));
    message.io = 2;
    message.value = toggle;

    esp_now_send(slave_mac, (uint8_t*)&message, sizeof(message));
    delay(1000);
}