#include <WiFiUdp.h>
// #include "ESP_Webpage.h"

struct UDP_Message {
    char source[18];
    char target[18];
    char topicCode;          
    int intValue;
    bool boolValue;
};

struct Host_Info {
    IPAddress ipaddr;
    char name[18];
};

UDP_Message espMessage;
UDP_Message espMessage1;

const int udpPort = 4210;
WiFiUDP udp;

Host_Info hostsList[100];
int currentIpIndex = 0;
char* hostIDs = new char[100];

char tagNames[10][20];
char currentTagIndex = 1;

bool arrayContains2(IPAddress target) {
    if (currentIpIndex == 0) { return false; }

    for (int i=0; i<currentIpIndex; i++) {
        if (hostsList[i].ipaddr == target) {
            return true;
        }
    }

    return false;   
}

bool arrayContains(char *array, char target) {
    if (currentIpIndex == 0) { return false; }

    for (int i=0; i<currentIpIndex; i++) {
        if (array[i] == target) {
            return true;
        }
    }

    return false;
}

void loadMacAddress(uint8_t mac[]) {
    sprintf(tagNames[0], "ESP-%02X%02X%02X", mac[3], mac[4], mac[5]);
    Serial.print("\nDeviceName: "); Serial.println(tagNames[0]);
}

void editTagName(String oldName, String newName) {
    char oldNameChar[20];
    oldName.toCharArray(oldNameChar, 20);

    for (int i=0; i<currentTagIndex; i++) {
        char *target = tagNames[i];

        if (strcmp(target, oldNameChar) == 0) {
            char newNameChar[20];
            newName.toCharArray(newNameChar, 20);
            strncpy(tagNames[i], newNameChar, 20);
        }
    }
}

void deleteTagName(String name) {
    if (currentTagIndex < 1) { return; }
    char tagName[20];
    name.toCharArray(tagName, 20);

    for (int i=0; i<currentTagIndex; i++) {
        char *target = tagNames[i];

        if (strcmp(target, tagName) == 0) {
            for (int j=0; j+1<currentTagIndex; j++) {
                strncpy(tagNames[i], tagNames[j+1], 20);
            }

            currentTagIndex--;
            return;
        }
    }
}

void addTagName(String name) {
    char tagName[20];
    name.toCharArray(tagName, 20);
    if (strlen(tagName) < 4 || currentTagIndex > 9 ) { return; }

    for (int i=0; i<currentTagIndex; i++) {
        char *target = tagNames[i];

        if (strcmp(target, tagName) == 0) {
            return;
        } 
    }

    strncpy(tagNames[currentTagIndex], tagName, 20);
    currentTagIndex++;
    // tagNames[arrLength] = tagName;
}

String getTagsList() {
    String output = "";

    for (int i=0; i<currentTagIndex; i++) {
        char *match = tagNames[i];

        output += FPSTR("<div style=\"display:table; width:100%\">");
        // output += "<a href=\"http://" + match.ipaddr.toString() + "\" style=\"font-size:40px; text-align:left; display:table-cell;\">" + match.ipaddr.toString() + "</a>";

        output += FPSTR("<label id=\"nameStr\" style=\"font-size:40px; text-align:left; display:table-cell;\">");
        output += match;
        output += FPSTR("</label>");

        output += FPSTR("<div style=\"text-align:right\">");
        output += FPSTR("<button onclick=\"htmlOnSetTag('");
        output += match;
        output += FPSTR("'); return false;\" ");
        output += FPSTR("style=\"display:table-cell; text-align:center; background-color:#e7e7e7; height:150px; font-size:70px; border: 2px solid black;\">Edit</button>");
        output += FPSTR("</div>");

        output += FPSTR("</div>");      
    }

    return output;
}

String getDevicesList() {
    String output = "";

    for (int i=0; i<currentIpIndex; i++) {
        Host_Info match = hostsList[i];
        
        output += FPSTR("<div style=\"display:table; width:100%\">");
        output += "<a href=\"http://" + match.ipaddr.toString() + "\" style=\"font-size:40px; text-align:left; display:table-cell;\">" + match.ipaddr.toString() + "</a>";

        output += FPSTR("<label id=\"nameStr\" style=\"font-size:40px; text-align:left; display:table-cell;\">");
        output += match.name;
        output += FPSTR("</label>");

        output += FPSTR("<div style=\"text-align:right\">");
        output += FPSTR("<button onclick=\"httpSendCommand('");
        output += match.name;
        output += FPSTR("'); return false;\" ");
        output += FPSTR("style=\"display:table-cell; text-align:center; background-color:#e7e7e7; height:150px; font-size:70px; border: 2px solid black;\">Toggle</button>");
        output += FPSTR("</div>");

        output += FPSTR("</div>");
    }
    
    return output;
}

String getDevicesManagerPage() {
    String page = FPSTR(HTTP_START);
    page += FPSTR(HTTP_MAIN_BTN);
    page += FPSTR(HTTP_FORM_START);
    page += FPSTR("<h1 style=\"text-align:center; font-size:50px;\">Devices List</h1>");
    page += FPSTR("<ul id=\"devicesList\">");
    page += getDevicesList();
    page += FPSTR("</ul>");
    page += FPSTR("<button type=\"button\" onclick=\"refreshDevices()\" style=\"font-size:70px; height:150px; width:95%; margin:30px;\">Refresh</button>");
    page += FPSTR(HTTP_FORM_END);

    page += FPSTR(HTTP_FORM_START);
    page += FPSTR("<div><h1 style=\"text-align:center; font-size:50px;\">Assign Tag</h1></div>");
    page += FPSTR("<ul id=\"tagsList\">");
    page += getTagsList();
    page += FPSTR("</ul>");
    page += FPSTR("<div id=\"modalContainer\"></div>");
    page += FPSTR("<div><button type=\"button\" onclick=\"htmlOnSetTag()\" style=\"font-size:70px; height:150px; width:95%; margin-top:50px;\">Create Tag</button></div>");
    page += FPSTR("<div><button type=\"button\" onclick=\"htmlRestartESP()\" style=\"font-size:70px; height:150px; width:95%; margin-top:50px; margin-bottom:50px;\">Restart ESP</button></div>");
    page += FPSTR(HTTP_FORM_END);

    page += FPSTR(HTTP_FORM_START);
    page += FPSTR("<div><h1 style=\"text-align:center; font-size:50px;\">Update Firmware</h1></div>");
    page += FPSTR("<div id=\"confirmModalContainer\"></div>");
    page += FPSTR("<div><button type=\"button\" onclick=\"htmlOnUpdateFirmware()\" style=\"font-size:70px; height:150px; width:95%; margin:50px;\">Update</button></div>");
    page += FPSTR(HTTP_FORM_END);


    page += FPSTR(HTTP_DEVICEMNG_SCRIPT);
    page += FPSTR(HTTP_END);

    return page;
}

void sendUDP(UDP_Message message, IPAddress sourceIP, IPAddress targetIP) {
    Serial.print("\nsendUDP");    
    udp.beginPacketMulticast(targetIP, udpPort, sourceIP);
    udp.write((char*) &message, sizeof(message));
    udp.endPacket();
}

void sendUDP(UDP_Message message, IPAddress sourceIP) {
    IPAddress broadcast2 = IPAddress(sourceIP[0], sourceIP[1], sourceIP[2], 255);
    Serial.println(broadcast2.toString());
    sendUDP(message, sourceIP, broadcast2);
}

void requestDevicesList(IPAddress sourceIP) {
    strncpy(espMessage1.source, tagNames[0], sizeof(espMessage1.source));
    strncpy(espMessage1.target, "", sizeof(espMessage1.target));

    espMessage1.intValue = 12345678;
    espMessage1.boolValue = true;
    espMessage1.topicCode = 127;
    sendUDP(espMessage1, sourceIP);
}

void processCommand(String data, IPAddress sourceIP) {
    int str_len = data.length() + 1;
    data.toCharArray(espMessage1.target, str_len);
    strncpy(espMessage1.source, tagNames[0], sizeof(espMessage1.source));

    espMessage1.intValue = 12345678;
    espMessage1.boolValue = true;
    espMessage1.topicCode = 100;

    sendUDP(espMessage1, sourceIP);
}

void receivedPacket(IPAddress sourceIP) {
    char *tagName = tagNames[0];
    int packetSize = udp.parsePacket();

    if (packetSize) {
        char* bufIP = new char[8];
        
        IPAddress remoteIP = udp.remoteIP();
        sprintf(bufIP, "%d.%d.%d.%d", remoteIP[0], remoteIP[1], remoteIP[2], remoteIP[3]);
        Serial.printf("\nReceived %d bytes from %s, port %d\n", packetSize, remoteIP.toString().c_str(), udp.remotePort());
        int len = udp.read((char*) &espMessage1, sizeof(espMessage1));

        Serial.println("BufIP: " + String(bufIP));
        Serial.print("Source: "); Serial.println(espMessage1.source);
        Serial.print("Target: "); Serial.println(espMessage1.target);
        Serial.printf("\nTopicCode: %d", espMessage1.topicCode);
        Serial.printf("\nIntVal: %d", espMessage1.intValue);
        Serial.printf("\nBoolVal: %d", espMessage1.boolValue);
        // digitalWrite(2, !digitalRead(2));

        bool reading1 = arrayContains2(remoteIP);

        if (!reading1) {
            Host_Info hostInfo;
            hostInfo.ipaddr = remoteIP;
            strncpy(hostInfo.name, espMessage1.source, sizeof(hostInfo.name));
            hostsList[currentIpIndex] = hostInfo;
            currentIpIndex++;
        }
        
        udp.flush();

        if (espMessage1.topicCode == 127) {
            UDP_Message respMessage;
            strncpy(respMessage.source, tagName, sizeof(respMessage.source));
            strncpy(respMessage.target, "", sizeof(respMessage.target));
            respMessage.topicCode = 100;
            respMessage.intValue = 99999;
            respMessage.boolValue = false;
            sendUDP(respMessage, sourceIP, remoteIP);           

        } else if (strcmp(espMessage1.target, tagName) == 0) {
            digitalWrite(2, !digitalRead(2));
            Serial.println("DOREMON");
        }
    }  
}
