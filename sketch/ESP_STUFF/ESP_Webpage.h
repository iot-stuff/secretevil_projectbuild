char HTTP_START[] PROGMEM = "<html><head></head><body>";
char HTTP_END[] PROGMEM = "</body></html>";
char HTTP_MAIN_BTN[] PROGMEM = "<a href=\"/\"><button style=\"font-size:80px; height:150px; width:95%; margin-top:50px; display:flex; justify-content:center;\">Main</button></a>";
char HTTP_FORM_START[] PROGMEM = "<form style=\"margin-top:50px;\"><fieldset>";
char HTTP_FORM_END[] PROGMEM = "</fieldset></form>";
char HTTP_TITLE[] PROGMEM = "<h1 style=\"text-align:center; font-size:50px;\">{v}</h1>";

char HTTP_GPIOS_SCRIPT[] PROGMEM = R"=====(
	<script>
		function setGPIO(io, value) {
			// var data = {io:io, value:value};
			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if(xhr.responseText != null){
						document.getElementById('devicesList').innerHTML = xhr.responseText;
						// console.log(xhr.responseText);
					}
				}
			};
			xhr.open("GET", "/refreshDevices", true);
			xhr.send();
			// xhr.send(JSON.stringify(data));			
		}
	</script>
)=====";

char HTTP_DEVICEMNG_SCRIPT[] PROGMEM = R"=====(
	<script>
		function clearTextField() {
			document.getElementById("tagName").value = "";
		}

		function httpSendCommand(output) {
			var xhr = new XMLHttpRequest();
			// var hostName = document.getElementById("hostName").value;

			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if(xhr.responseText != null){
						console.log(xhr.responseText);
					}
				}
			};

			xhr.open("POST", "/sendCommand", true);
			xhr.send(output); 
		}

		function setGPIO(io, value) {
			// var data = {io:io, value:value};
			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if(xhr.responseText != null){
						document.getElementById('devicesList').innerHTML = xhr.responseText;
						// console.log(xhr.responseText);
					}
				}
			};
			xhr.open("GET", "/refreshDevices", true);
			xhr.send();
			// xhr.send(JSON.stringify(data));			
		}

		function refreshDevices() {
			var xhr = new XMLHttpRequest();

			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if(xhr.responseText != null){
						document.getElementById('devicesList').innerHTML = xhr.responseText;
						// console.log(xhr.responseText);
					}
				}
			};
			
			xhr.open("GET", "/refreshDevices", true);
			xhr.send();	
		}

		function htmlRestartESP() {
			var xhr = new XMLHttpRequest();
			xhr.open("GET", "/restartESP", true);
			xhr.send();
		}

		function htmlRefeshTags() {
			var xhr = new XMLHttpRequest();
			var tagName = document.getElementById("tagName").value;

			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if(xhr.responseText != null){
						document.getElementById('tagLists').innerHTML = xhr.responseText;
						// console.log(xhr.responseText);
					}
				}
			};

			xhr.open("POST", "/refreshTags", true);
			xhr.send();
		}

		function htmlCloseConfirmModal() {
			document.getElementById('confirmModalContainer').innerHTML = "";
		}

		function htmlOnUpdateFirmware() {
			document.getElementById('confirmModalContainer').innerHTML = "<div style=\"position:fixed; top:30%; left:10%; width:80%; height:650px; background-color:#f1c40f; text-align:center; z-index:10; outline:9999px solid rgba(0,0,0,0.5);\">" +
				"<div><h1 style=\"text-align:center; font-size:50px;\">Are you sure?</h1></div>" +
					"<button type=\"button\" onclick=\"htmlStartUpdateFirmware()\" style=\"font-size:40px; width:95%; height:100px; margin:10px;\">Yes</button>" +
					"<button type=\"button\" onclick=\"htmlCloseConfirmModal()\" style=\"font-size:40px; width:95%; height:100px; margin:10px;\">No</button>" +				
			"</div>"
		}

		function htmlStartUpdateFirmware() {
			var xhr = new XMLHttpRequest();
			xhr.open("POST", "/updateFirmware", true);
			xhr.send();
		}

		function htmlOnSetTag(tagName) {
			if (tagName==null) {
				document.getElementById('modalContainer').innerHTML = "<div style=\"position:fixed; top:30%; left:10%; width:80%; height:650px; background-color:#f1c40f; text-align:center; z-index:10; outline:9999px solid rgba(0,0,0,0.5);\">" +
					"<div><h1 style=\"text-align:center; font-size:50px;\">Create Tag</h1></div>" +
					"<div style=\"display:flex;\">" +
						"<input id=\"tagName\" placeholder=\"Tag Name\" value=\"\" style=\"flex:1; width:100%; height:100px; font-size:60px; margin-bottom:20px;\">" + 
						"<button type=\"button\" onclick=\"clearTextField()\" style=\"font-size:40px;\">X</button>" +
					"</div>" +
					"<label id=\"hostNameStatus\" style=\"font-size:40px;\">Status</label>" +
					"<button type=\"button\" onclick=\"htmlCreateTag()\" style=\"font-size:40px; width:95%; height:100px; margin:10px;\">Create</button>" +
					"<button type=\"button\" onclick=\"htmlCloseTagModal()\" style=\"font-size:40px; width:95%; height:100px; margin:10px;\">Cancel</button>" +
				"</div>";
			} else {
				document.getElementById('modalContainer').innerHTML = "<div style=\"position:fixed; top:30%; left:10%; width:80%; height:650px; background-color:#f1c40f; text-align:center; z-index:10; outline:9999px solid rgba(0,0,0,0.5);\">" +
					"<h1 style=\"text-align:center; font-size:50px;\">Edit Tag</h1>" +
					"<div style=\"display:flex;\">" +
						"<input id=\"tagName\" placeholder=\"Tag Name\" value=\"" + tagName + "\" style=\"flex:1; width:100%; height:100px; font-size:60px; margin-bottom:20px;\">" + 
						"<button type=\"button\" onclick=\"clearTextField()\" style=\"font-size:40px;\">X</button>" +
					"</div>" +
					"<label id=\"hostNameStatus\" style=\"font-size:40px;\">Status</label>" +
					"<button type=\"button\" onclick=\"htmlEditTag('" + tagName + "')\" style=\"font-size:40px; width:95%; height:100px; margin:10px;\">Save</button>" +
					"<button type=\"button\" onclick=\"htmlDeleteTag('" + tagName + "')\" style=\"font-size:40px; width:95%; height:100px; margin:10px;\">Delete</button>" +
					"<button type=\"button\" onclick=\"htmlCloseTagModal()\" style=\"font-size:40px; width:95%; height:100px; margin:10px;\">Cancel</button>" +
				"</div>";
			}
		}

		function htmlCloseTagModal() {
			document.getElementById('modalContainer').innerHTML = "";
		}

		function htmlDeleteTag(tagName) {
			htmlCloseTagModal();
			var xhr = new XMLHttpRequest();
			
			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if(xhr.responseText != null){
						document.getElementById('tagsList').innerHTML = xhr.responseText;
						// console.log(xhr.responseText);
					}
				}
			}

			xhr.open("POST", "/deleteTagName", true);
			xhr.send(tagName);
		}

		function htmlEditTag(oldTagName) {
			var xhr = new XMLHttpRequest();
			var newTagName = document.getElementById("tagName").value;
			var jsonResp = {old:oldTagName, new:newTagName};

			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if(xhr.responseText != null){
						document.getElementById('tagsList').innerHTML = xhr.responseText;
						htmlCloseTagModal();
						// console.log(xhr.responseText);
					}
				}
			};

			xhr.open("POST", "/editTagName", true);
			xhr.send(JSON.stringify(jsonResp));			
			
		}

		function htmlCreateTag() {
			var tagName = document.getElementById("tagName").value;
			return htmlSaveTag(tagName);
		}

		function htmlSaveTag(tagName) {
			var xhr = new XMLHttpRequest();

			if (tagName.trim().length > 3 == false) {
				document.getElementById("hostNameStatus").innerHTML = 'Error: Invalid tagName';
				document.getElementById("hostNameStatus").style.backgroundColor = "orange";
				return false;
			} else {
				document.getElementById("hostNameStatus").innerHTML = 'Success';
				document.getElementById("hostNameStatus").style.backgroundColor = "green";
				htmlCloseTagModal();

				xhr.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
						if(xhr.responseText != null){
							document.getElementById('tagsList').innerHTML = xhr.responseText;
							// console.log(xhr.responseText);
						}
					}
				};

				xhr.open("POST", "/setTagName", true);
				xhr.send(tagName);
				return true;
			}
		}
	</script>
)=====";

char mainPage[] PROGMEM = R"=====(
<html>
<head>
</head>
	<body>
		<div style="margin-top:50px;">
			<a href="/infoPage"><button style="display:inline-block; width:45%; height:150px; font-size:70px;">Info</button></a>
			<a href="/gpiosPage"><button style="display:inline-block; width:45%; height:150px; margin-left:20px; font-size:70px;">GPIOs</button></a>
		</div>
		<button style="font-size:70px; height:150px; width:95%; margin-top:30px; margin-bottom:30px;" onclick="toggleIndicator()">Toggle Indicator</button>
		<a href="/devicesManagerPage"><button style="font-size:70px; height:150px; width:95%; margin-bottom:50px;">Devices Manager</button></a>
		<form>
			<fieldset>
			<div>
				<h1 style="text-align:center; font-size:50px;">Connecting to Wifi network</h1>	
			</div>
			<label>SSID</label>      
			<div>
				<input id="hostSSID" placeholder="SSID" value=""
				style="width:100%; height:100px; font-size:60px; margin-bottom:20px;">
			</div>
			<label>PASSWORD</label>
			<div>
				<input id="hostPassword" placeholder="PASSWORD" type="password" value="" 
				style="width:100%; height:100px; font-size:60px;">
			</div>
			<label id="hostStatus" style="font-size:40px;">Status</label>
			<div>
				<button class="primary" id="savebtn" type="button" onclick="hostConfig()" 
				style="font-size:70px; height:150px; width:95%; margin-top:50px; margin-bottom:50px;">Submit</button>
			</div>
			</fieldset>
		</form>

		<form style="margin-top:50px;">
			<fieldset>
			<div>
				<h1 style="text-align:center; font-size:50px;">Configure Access Point</h1>	
			</div>
			<label>SSID</label>      
			<div>
				<input id="apSSID" placeholder="SSID" value=""
				style="width:100%; height:100px; font-size:60px; margin-bottom:20px;">
			</div>
			<label>PASSWORD</label>
			<div>
				<input id="apPassword" placeholder="PASSWORD" type="password" value=""
				style="width:100%; height:100px; font-size:60px;">
			</div>
			<label id="apStatus" style="font-size:40px;">Status</label>
			<div>
				<button id="factoryAPbtn" type="button" onclick="location.href='/factoryAP'" 
				style="font-size:70px; height:150px; width:95%; margin-top:30px; margin-bottom:10px;">Factory AP</button>
			</div>
			<div>
				<button type="button" onclick="apConfig()" 
				style="font-size:70px; height:150px; width:95%; margin-top:50px; margin-bottom:50px;">Submit</button>
			</div>
			</fieldset>
		</form>
	</body>

	<script>
		function toggleIndicator() {
			var xhr = new XMLHttpRequest();
			xhr.open("GET", "/toggleIndicator", true);
			xhr.send();
		}

		function sendRequest(url, ssid, password) {
			var data = {ssid:ssid, password:password};
			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if(xhr.responseText != null){
						// document.getElementById('hostStatus').innerHTML = xhr.responseText;
						console.log(xhr.responseText);
					}
				}
			};
			xhr.open("POST", url, true);
			xhr.send(JSON.stringify(data));
		}

		function checkTextFields(target, ssid, password) {
			if (ssid.trim().length > 0 == false) {
				document.getElementById(target).innerHTML = 'Error: Invalid SSID';
				document.getElementById(target).style.backgroundColor = "orange";
				return false;
			} else if (password.trim().length > 7 == false) {
				document.getElementById(target).innerHTML = 'Error: Invalid Password';
				document.getElementById(target).style.backgroundColor = "orange";
				return false;
			} else {
				document.getElementById(target).innerHTML = 'Success';
				document.getElementById(target).style.backgroundColor = "green";
				return true;
			}
		}

		function hostConfig() {
			console.log("button was clicked!");
			var ssid = document.getElementById("hostSSID").value;
			var password = document.getElementById("hostPassword").value;
			
			if (checkTextFields('hostStatus', ssid, password)) {
				sendRequest("/configureHost", ssid, password);
			}
		};

		function apConfig() {
			console.log("button was clicked!");
			var ssid = document.getElementById("apSSID").value;
			var password = document.getElementById("apPassword").value;

			if (checkTextFields('apStatus', ssid, password)) {
				sendRequest("/configureAP", ssid, password);
			}
		};
	</script>
</html>
)=====";

char infoPage[] PROGMEM = R"=====(
	<!DOCTYPE HTML>
	<html>
	<head></head>
	<body>
		<a href="/"><button style="font-size:80px; height:150px; width:95%; margin-top:50px;">Main</button></a>
		<h1 style="text-align:center; font-size:50px; margin-top:50px;">Unit Info</h1>

		<p style="font-size:30px; text-align:left; width:49%; display: inline-block;"><b>Uptime</b></p>
		<p style="font-size:30px; text-align:right; width:49%; display: inline-block;">0 Mins 44 Secs</p>
		<p style="font-size:30px; text-align:left; width:49%; display: inline-block;"><b>AP IP</b></p>
		<p style="font-size:30px; text-align:right; width:49%; display: inline-block;">192.168.4.1</p>
		<p style="font-size:30px; text-align:left; width:49%; display: inline-block;"><b>AP MAC</b></p>
		<p style="font-size:30px; text-align:right; width:49%; display: inline-block;">00:00:00:00:00:00</p>		
		<p style="font-size:30px; text-align:left; width:49%; display: inline-block;"><b>Router MAC</b></p>
		<p style="font-size:30px; text-align:right; width:49%; display: inline-block;">00:00:00:00:00:00</p>
		<p style="font-size:30px; text-align:left; width:49%; display: inline-block;"><b>HostName</b></p>
		<p style="font-size:30px; text-align:right; width:49%; display: inline-block;">00:00:00:00:00:00</p>

		<p style="font-size:30px; text-align:left; width:49%; display: inline-block;"><b>Flash ChipID</b></p>
		<p style="font-size:30px; text-align:right; width:49%; display: inline-block;">00:00:00:00:00:00</p>
		<p style="font-size:30px; text-align:left; width:49%; display: inline-block;"><b>Flash Size</b></p>
		<p style="font-size:30px; text-align:right; width:49%; display: inline-block;">00:00:00:00:00:00</p>
		<p style="font-size:30px; text-align:left; width:49%; display: inline-block;"><b>SDK Version</b></p>
		<p style="font-size:30px; text-align:right; width:49%; display: inline-block;">00:00:00:00:00:00</p>
		<p style="font-size:30px; text-align:left; width:49%; display: inline-block;"><b>Core Version</b></p>
		<p style="font-size:30px; text-align:right; width:49%; display: inline-block;">00:00:00:00:00:00</p>
		<p style="font-size:30px; text-align:left; width:49%; display: inline-block;"><b>Boot Version</b></p>
		<p style="font-size:30px; text-align:right; width:49%; display: inline-block;">00:00:00:00:00:00</p>
		<p style="font-size:30px; text-align:left; width:49%; display: inline-block;"><b>CPU Frequency</b></p>
		<p style="font-size:30px; text-align:right; width:49%; display: inline-block;">00:00:00:00:00:00</p>
		<p style="font-size:30px; text-align:left; width:49%; display: inline-block;"><b>Memory-Free Heap</b></p>
		<p style="font-size:30px; text-align:right; width:49%; display: inline-block;">00:00:00:00:00:00</p>
		<p style="font-size:30px; text-align:left; width:49%; display: inline-block;"><b>Memory-Sketch Size</b></p>
		<p style="font-size:30px; text-align:right; width:49%; display: inline-block;">00:00:00:00:00:00</p>
	</body>
	</html>
)=====";

char gpiosPage[] PROGMEM = R"=====(
	<!DOCTYPE HTML>
	<html>
	<head></head>
	<body>
		<a href="/"><button style="font-size:80px; height:150px; width:95%; margin-top:50px; margin-bottom:50px;">Main</button></a>

		<form style="margin-top:50px;">
			<fieldset>
			<div>
				<h1 style="text-align:center; font-size:50px;">Test GPIOs</h1>	
			</div>
			</fieldset>
		</form>

		<form style="margin-top:50px;">
			<fieldset>
			<div>
				<h1 style="text-align:center; font-size:50px;">Configure GPIOs</h1>	
			</div>
			<div>
				<p style="font-size:40px; text-align:left; width:35%; display: inline-block;"><b>GPIO0</b></p>
				<label class="radio-inline" style="font-size:50px;">
					<input type="radio" style="font-size:50px; width:40px; height:40px;" name="optradio" checked> Input
				</label>
				<label class="radio-inline" style="font-size:50px;">
					<input type="radio" style="font-size:50px; width:40px; height:40px;" name="optradio"> Output
				</label>
				<label class="radio-inline" style="font-size:50px;">
					<input type="radio" style="width:40px; height:40px;" name="optradio"> None
				</label>
			</div>
			<div>
				<p style="font-size:40px; text-align:left; width:35%; display: inline-block;"><b>GPIO1</b></p>
				<label class="radio-inline" style="font-size:50px;">
					<input type="radio" style="font-size:50px; width:40px; height:40px;" name="optradio" checked> Input
				</label>
				<label class="radio-inline" style="font-size:50px;">
					<input type="radio" style="font-size:50px; width:40px; height:40px;" name="optradio"> Output
				</label>
				<label class="radio-inline" style="font-size:50px;">
					<input type="radio" style="width:40px; height:40px;" name="optradio"> None
				</label>
			</div>
			<div>
				<p style="font-size:40px; text-align:left; width:35%; display: inline-block;"><b>GPIO2</b></p>
				<label class="radio-inline" style="font-size:50px;">
					<input type="radio" style="font-size:50px; width:40px; height:40px;" name="optradio" checked> Input
				</label>
				<label class="radio-inline" style="font-size:50px;">
					<input type="radio" style="font-size:50px; width:40px; height:40px;" name="optradio"> Output
				</label>
				<label class="radio-inline" style="font-size:50px;">
					<input type="radio" style="width:40px; height:40px;" name="optradio"> None
				</label>
			</div>	
			</fieldset>
		</form>
	</body>
	</html>
)=====";

// char webpage[] PROGMEM = R"=====(
// 	<!DOCTYPE HTML>
// 	<html>
// 	<head></head>
// 	<body>
// 	<form>
// 	<div>
// 		<label for="ssid">SSID*</label>
// 		<input value="" id="ssid" placeholder="SSID"/>
// 	</div>
// 	<div>
// 		<label for="password">PASSWORD*</label>
// 		<input value="" type="password" id="password" placeholder="PASSWORD"/>
// 	</div>
// 	<div>
// 		<button onclick="myFunction()"> SAVE </button>
// 	</div>
// 	</form>
// 	</body>
	
// 	<script>
// 	function myFunction()
// 	{
// 		console.log("button was clicked!");

// 		var ssid = document.getElementById("ssid").value;
// 		var password = document.getElementById("password").value;
// 		var data = {ssid:ssid, password:password};

// 		var xhr = new XMLHttpRequest();
// 		var url = "/settings";

// 		xhr.onreadystatechange = function() {
// 		if(this.onreadyState == 4 && this.status == 200) {
// 			console.log(xhr.responseText);
// 		}
// 		};

// 		xhr.open("POST", url, true);
// 		xhr.send(JSON.stringify(data));
// 	};
// 	</script>
// 	</html>
// )=====";