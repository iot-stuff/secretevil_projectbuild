#include <FS.h>
// #include "SPIFFS.h"

struct KVPModel {
    String key;
    String value;
};

void mountSPIFFS() {
    SPIFFS.begin() ? Serial.println("*DEBUG: SPIFFS mounted") : 
        Serial.println("*DEBUG Error: SPIFFS mount failed");
}

void formatAllFiles() {
    SPIFFS.format();
}

void listAllFiles() {
    // Dir dir = SPIFFS.openDir("/");
    // // std::list<String> list;

    // while(dir.next()) {
    //     File entry = dir.openFile("r");
    //     // list.push_back(entry.name());
    //     Serial.println("*DEBUG: SPIFFS file"); Serial.print(entry.name());
    //     entry.close();
    // }

    // Serial.println();

    // // return list;
}

void writeSPIFFS(String path, String text) {
    File file = SPIFFS.open(path, "w");

    if (!file) {
        Serial.println("[DEBUG] fail to open file");
        file.close();
        return;
    }

    if (file.print(text) == 0) {
        Serial.println("[DEBUG] fail to write file");
    }

    Serial.println("\n[DEBUG] saved file");
    file.close();
}

bool readSPIFFS(String path, char *buffer) {
    bool output = true;
    Serial.println("[DEBUG] Read SPIFFS");
    File file = SPIFFS.open(path, "r");

    if (!file) {
        Serial.println("[DEBUG] fail to open file");
        output = false;
    }

    for (int i=0; i<file.size(); i++) {
        buffer[i] = (char)file.read();
    }

    file.close();
    return output;
}

bool readLineSPIFFS(String path, char buffer[][20]) {
    Serial.println("[DEBUG] Readline SPIFFS");
    File file = SPIFFS.open(path, "r");

    if (!file) {
        Serial.println("[DEBUG] fail to open file");
        file.close();
        return false;
    }

    char lineBuff[20];
    while (file.available()) {
        // Serial.print((char)file.read());
        int l = file.readBytesUntil('\n', lineBuff, sizeof(lineBuff));
        lineBuff[l] = 0;
        Serial.printf("\nBUFF: %s", lineBuff);
    }

    file.close();
    return true;
}
/////////////////////////

void writeKVP_SPIFFS(String path, String key, String value) {
    File file = SPIFFS.open(path, "w");

    if (!file) {
        Serial.println("*DEBUG Error: fail to open file");
        return;
    }

    if (file.println(key) == 0) {
        Serial.println("*DEBUG Error: fail to write file");
    }

    if (file.println(value) == 0) {
        Serial.println("*DEBUG Error: fail to write file");
    }

    Serial.println("*DEBUG: saved file");
    file.close();
}

KVPModel readKVP_SPIFFS(String path) {
    KVPModel kvp;
    File file = SPIFFS.open(path, "r");
    int i=0;

    while (file.available()) {
        if(i==0) {
            kvp.key = file.readStringUntil('\n');
            i++;
        } else {
            kvp.value = file.readStringUntil('\n');
        }
    }

    file.close();
    return kvp;
}
