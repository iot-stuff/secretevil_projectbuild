#include <ArduinoJson.h>
#include <DNSServer.h>
#include "ESP_Webpage.h"
#include "ESP_UDP.h"
#include <ESP8266httpUpdate.h>
#include "../MANAGERS/File_Manager.h"

extern "C" {
    #include<user_interface.h>
}

#define ESP8266

#if defined(ESP8266) 
    #include <ESP8266WiFi.h>
    #include <ESP8266WebServer.h>
    #include <ESP8266HTTPClient.h>
    ESP8266WebServer webServer(80);
#else
    #include <WiFi.h>
    #include <WebServer.h>
    WebServer webServer(80);
#endif

IPAddress apIP(192, 168, 28, 1);
DNSServer dnsServer;
const byte DNS_PORT = 53;

char buffer[100];
StaticJsonDocument<100> doc;
char boardName[20];

void restoreFactoryAP() {
    String defaultAp =  "{\"ssid\":\"beehive\", \"password\":\"333666999\"}";
    writeSPIFFS("/configureAP.txt", defaultAp);
}

void restoreFactoryInfo() {
    
    // String defaultInfo = {\"name\":\""\};
    // writeSPIFFS("/info.text", defaultInfo);
}

void somethingSomething() {
    unsigned char mac[6];
    WiFi.macAddress(mac);
    Serial.print(mac[0], 16); Serial.print("."); 
    Serial.print(mac[1], 16); Serial.print("."); 
    Serial.print(mac[2], 16); Serial.print("."); 
    Serial.print(mac[3], 16); Serial.print("."); 
    Serial.print(mac[4], 16); Serial.print("."); 
    Serial.print(mac[5], 16); Serial.print("."); 
}

void loadTagName() {
    Serial.println("[DEBUG] Load TagName");  

    char tagsBuffer[200];
    bool reading = readSPIFFS("/tagNamesList.txt", tagsBuffer);

}

void getWifiList() {
    int numberOfNetworks = WiFi.scanNetworks();

    for(int i =0; i<numberOfNetworks; i++){
        Serial.print("Network name: ");
        Serial.println(WiFi.SSID(i));
        Serial.print("Signal strength: ");
        Serial.println(WiFi.RSSI(i));
        Serial.println("-----------------------");
    }
}

void loadAccessPoint() {
    Serial.println("[DEBUG] Load Accesspoint");  

    bool reading = readSPIFFS("/configureAP.txt", buffer);
    if (!reading) {
        Serial.println(F("[DEBUG] No valid AP Config"));
        restoreFactoryAP();
        ESP.restart();
    }

    deserializeJson(doc, buffer);
    String ssid1 = doc["ssid"];
    String password1 = doc["password"];

    if (ssid1.length() < 1 || password1.length() < 8) {
        Serial.println(F("[DEBUG] No valid AP Config"));
        restoreFactoryAP();
        ESP.restart();
    } else {
    //     // Serial.print("[DEBUG] Start AP: "); Serial.println(ssid);
        Serial.print("SSID: "); Serial.println(ssid1);
        Serial.print("Pass: "); Serial.println(password1);
        WiFi.mode(WIFI_AP);
        WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
        WiFi.softAP(ssid1, password1, false);  
        Serial.print("[DEBUG] AP IP "); Serial.println(WiFi.softAPIP());
    }
}

// char tagNames[20][15];

void loadHost() {
    char hostBuffer[100];
    Serial.println("[DEBUG] Load Host");
    // bool read = readSPIFFS("/configureHost.txt", hostBuffer);
    // if (!read) { return; }

    // deserializeJson(doc, hostBuffer);
    // String ssid2 = doc["ssid"];
    // String password2 = doc["password"];
    // Serial.println(ssid); Serial.println(password);

    // if (ssid2.length() < 1 || password2.length() < 8) {
    //     Serial.println(F("[DEBUG] No valid Host Config"));
    // } else {
        Serial.print(F("\n[DEBUG] Connecting to Host"));
        unsigned long startTime = millis();
        WiFi.mode(WIFI_AP_STA);
        WiFi.begin("Orchid-2.4", "Orchid8279");
        // WiFi.begin("CaliNails1450", "2604590712");
        // WiFi.begin("Captain America", "vec97sy1");
        // WiFi.begin("Captain China", "Vec97sy1");
        // WiFi.begin("FA_Nails", "fanails1618");
        // WiFi.begin("NETGEAR26", "0001112223");
    //     WiFi.begin(ssid, password);

        while(WiFi.status() != WL_CONNECTED) {
            delay(500);
            Serial.print(".");
            if(millis() - startTime > 15000) break;
        }

        if (WiFi.status() != WL_CONNECTED) {
            Serial.println(F("\n[DEBUG] Timeout connecting to host"));
        } else {
            Serial.println(F("\n[DEBUG] Connected"));  
            uint8_t mac[6];
            WiFi.macAddress(mac);
            loadMacAddress(mac);
            Serial.print("\n[DEBUG] LocalIP:"); Serial.println(WiFi.localIP());
        }
    // }
}

void loadTagNames() {

}

void storeTagNames() {
    String saveString = "";
    for (int i=0; i<currentTagIndex; i++) {
        saveString = saveString + tagNames[i] + '\n';
    }

    writeSPIFFS("/tagNamesList.txt", saveString);
    readLineSPIFFS("/tagNamesList.txt", tagNames);
}

void setupWebserver() {
    Serial.println(F("*DEBUG: Setup Webserver"));
    // if DNSServer is started with "*" for domain name, it will reply with
    // provided IP to all DNS request
    dnsServer.start(DNS_PORT, "*", apIP);

   // captive webserver
    webServer.onNotFound([]() {
        webServer.send_P(200, "text/html", mainPage);
    });

    webServer.on("/", []() {
        Serial.println("IM CALLED");
        webServer.send_P(200, "text/html", mainPage);
    });

    webServer.on("/factoryAP", []() {
        Serial.println(F("[DEBUG] /factoryAP called"));
        restoreFactoryAP();
    });

    webServer.on("/infoPage", []() {
        webServer.send_P(200, "text/html", infoPage);
    });

    webServer.on("/gpiosPage", []() {
        int gpios[] = {0, 1, 2, 3, 4, 5, 6, 12, 13, 14, 15, 16};
        String page = FPSTR(HTTP_START);

        page += FPSTR(HTTP_MAIN_BTN);
        page += FPSTR(HTTP_FORM_START);
        page += FPSTR("<h1 style=\"text-align:center; font-size:50px;\">Test GPIOs</h1>");

        for (int i=0; i<sizeof(gpios)/sizeof(gpios[0]); i++) {
            page += FPSTR("<div><p style=\"font-size:40px; text-align:left; width:25%; display:inline-block;\"><b>");
            page += "GPIO" + String(gpios[i]);
            page += FPSTR("</b></p>");

            // page += FPSTR("<button onclick=\"setGPIO(1,1)\" style=\"display:inline-block; text-align:center; background-color:#e7e7e7; width:25%; height:150px; font-size:70px; border: 2px solid black;\">On</button>");

            page += "<button onclick=\"setGPIO(" + String(gpios[i]) + ", 1)\" ";
            page += FPSTR("style=\"display:inline-block; text-align:center; background-color:#e7e7e7; width:25%; height:150px; font-size:70px; border: 2px solid black;\">On</button>");
            page += "<button onclick=\"setGPIO(" + String(gpios[i]) + ", 0)\" ";
            page += FPSTR("style=\"display:inline-block; text-align:center; background-color:#e7e7e7; width:25%; height:150px; font-size:70px; border: 2px solid black;\">Off</button>");
            page += "<button onclick=\"setGPIO(" + String(gpios[i]) + ", 2)\" ";
            page += FPSTR("style=\"display:inline-block; text-align:center; background-color:#e7e7e7; width:25%; height:150px; font-size:70px; border: 2px solid black;\">Toggle</button>");
            page += FPSTR("</div>");
        }

        page += FPSTR(HTTP_FORM_END);

        page += FPSTR(HTTP_FORM_START);
        page += FPSTR("<h1 style=\"text-align:center; font-size:50px;\">Configure GPIOs</h1>");
        
        for (int i=0; i<sizeof(gpios)/sizeof(gpios[0]); i++) {
            page += FPSTR("<div><p style=\"font-size:40px; text-align:left; width:50%; display: inline-block;\"><b>");
            page += "GPIO" + String(gpios[i]);
            page += FPSTR("</b></p>");

            page += FPSTR("<label class=\"radio-inline\" style=\"font-size:50px;\"><input type=\"radio\" style=\"font-size:50px; width:40px; height:40px;\" name=\"optradio");
            page += String(i) + "\" checked> In. </label>";
            page += FPSTR("<label class=\"radio-inline\" style=\"font-size:50px;\"><input type=\"radio\" style=\"font-size:50px; width:40px; height:40px;\" name=\"optradio");
            page += String(i) + "\" checked> Out. </label>";
            page += FPSTR("<label class=\"radio-inline\" style=\"font-size:50px;\"><input type=\"radio\" style=\"font-size:50px; width:40px; height:40px;\" name=\"optradio");
            page += String(i) + "\" checked> None </label>";
            page += FPSTR("</div>");
        }

        page += FPSTR(HTTP_FORM_END);
        page += FPSTR("</body>");
        page += FPSTR(HTTP_GPIOS_SCRIPT);
        page += FPSTR("</html>");
        // page += FPSTR(HTTP_END);

        webServer.sendHeader("Content-Length", String(page.length()));
        webServer.send(200, "text/html", page);
    });

    webServer.on("/devicesManagerPage", []() {
        String page = getDevicesManagerPage();
        webServer.send(200, "text/html", page);
    });

    webServer.on("/refreshDevices", HTTP_GET, []() {
        Serial.println("\nIM CALLED");
        requestDevicesList(WiFi.localIP());
        webServer.send(200, "text/plain", getDevicesList());
    });

    webServer.on("/sendCommand", []() {
        Serial.println(F("[DEBUG] /sendCommand called"));
        String data = webServer.arg("plain");
        Serial.println(data);
        processCommand(data, WiFi.localIP());
        webServer.send(200, "text/plain", "OK");
    });

    webServer.on("/toggleIndicator", []() {
        Serial.println(F("[DEBUG] /toggleIndicator called"));
        digitalWrite(2, !digitalRead(2));
        digitalWrite(13, !digitalRead(13));
        webServer.send_P(200, "text/plain", "OK");
    });

    webServer.on("/updateFirmware", []() {
        Serial.println(F("\n[DEBUG] /updateFirmware called"));
        webServer.send_P(200, "text/plain", "OK");

        ESPhttpUpdate.onStart([]() {
            Serial.println("OTA Started:");
        });

        ESPhttpUpdate.onProgress([](int val1, int val2) {
            float percent = (float(val1)/float(val2))*100;
            Serial.println("OTA Progress: " + String(percent) + "%");
        });

        ESPhttpUpdate.onEnd([]() {
            Serial.println("OTA Ended");
            ESP.restart();
            // handleRestartESP();
        });

        ESPhttpUpdate.onError([](int err) {
            Serial.println("OTA Error: " + String(err));
        });

        t_httpUpdate_return ret = ESPhttpUpdate.update("http://download1082.mediafire.com/n4snl36lk1vg/xv16geyl07h5wwv/SecretEvil_Project.ino.bin");

        switch(ret) {
            case HTTP_UPDATE_FAILED:
                Serial.println("Error: " + ESPhttpUpdate.getLastErrorString());
                break;
            case HTTP_UPDATE_NO_UPDATES:
                Serial.println("Response: No update file");
                break;
            case HTTP_UPDATE_OK:
                Serial.println("Response: Update ok");
                break;
        }
    });

    webServer.on("/deleteTagName", []() {
        Serial.println(F("\n[DEBUG] /deleteTagName called"));
        String data = webServer.arg("plain");
        Serial.println(data);
        deleteTagName(data);
        storeTagNames();
        webServer.send(200, "text/plain", getTagsList());
    });

    webServer.on("/editTagName", []() {
        Serial.println(F("\n[DEBUG] /editTagName called"));
        String data = webServer.arg("plain");
        Serial.println(data);

        deserializeJson(doc, data.c_str());
        String oldTagName = doc["old"];
        String newTagName = doc["new"];

        if (oldTagName.length() > 4 && newTagName.length() > 4) {
            editTagName(oldTagName, newTagName);
        }

        webServer.send(200, "text/plain", getTagsList());
    });

    webServer.on("/setTagName", []() {
        Serial.println(F("\n[DEBUG] /setTagName called"));
        String data = webServer.arg("plain");
        Serial.println(data);
        addTagName(data);

        // storeTagNames();
        String saveString = "";

        for (int i=0; i<currentTagIndex; i++) {
            saveString = saveString + tagNames[i] + '\n';
        }

        writeSPIFFS("/tagNamesList.txt", saveString);
        readLineSPIFFS("/tagNamesList.txt", tagNames);

        webServer.send(200, "text/html", getTagsList());
    });

    // webServer.on("/refreshTags", []() {
    //     Serial.println(F("\n[DEBUG] /refreshTags called"));
    //     webServer.send(200, "text/html", getTagsList());
    // })

    webServer.on("/restartESP", []() {
        Serial.println(F("[DEBUG] /restartESP called"));
        webServer.send_P(200, "text/html", "OK");
    });

    webServer.on("/configureHost", []() {
        Serial.println(F("[DEBUG] /configureHost called"));
        webServer.send_P(200, "text/html", "OK");

        String data = webServer.arg("plain");
        Serial.print(data);
        // writeSPIFFS("/configureHost.txt", data);
        // ESP.restart();
    });

    webServer.on("/configureAP", []() {
        Serial.println(F("[DEBUG] /configureAP called"));
        webServer.send_P(200, "text/html", "OK");

        String data = webServer.arg("plain");
        // writeSPIFFS("/configureAP.txt", data);
        // ESP.restart();
    });

    webServer.on("/gpioWrite", []() {
        Serial.println(F("[DEBUG] /gpioWrite called"));
        webServer.send_P(200, "text/html", "OK");

        char buffer2[100];
        StaticJsonDocument<100> doc2;

        String data = webServer.arg("plain");
        Serial.println(data);
        // deserializeJson(doc2, buffer2);
        // const char *target = doc["target"].as<const char*>();
        // const char *io = doc["io"].as<const char*>();
        // const char *dValue = doc["dValue"].as<const char*>();

    });

    webServer.begin();

    udp.begin(4210);
}

struct station_info *sta_info;
struct ip4_addr *IPaddress;
IPAddress address;
HTTPClient httpClient;
WiFiClient wifiClient;

void dumpClients() {
    Serial.println("[DEBUG] Clients:");
    sta_info = wifi_softap_get_station_info();
    
    while (sta_info != NULL) {
        IPaddress = &sta_info->ip;
        address = IPaddress->addr;
        Serial.print("\t"); Serial.println(address);
        sta_info = STAILQ_NEXT(sta_info, next);

        String target = "http://" + address.toString() + "/test";
        httpClient.begin(wifiClient, target);
        int httpCode = httpClient.GET();

        if (httpCode > 0) {
            String payload = httpClient.getString();
            Serial.println("[DEBUG] response string" + payload);
        }
    }

    httpClient.end();
}

void accessPointStart() {
    getWifiList();
    
    pinMode(2, OUTPUT);
    pinMode(13, OUTPUT);

    Serial.println();
    mountSPIFFS();
    // formatAllFiles();

    loadAccessPoint();
    loadHost();
    setupWebserver();

    // Serial.print("MAC:"); Serial.println(WiFi.softAPmacAddress());
    // // WiFi.printDiag(Serial);
    // // listAllFiles();
}

void clientPingResponse() {
    // dnsServer.processNextRequest();
    webServer.handleClient();
    receivedPacket(WiFi.localIP());
    
    // dumpClients();
    // delay(2000);
}


// %%%%%%%%%%%%%%%%%%%%%
// CLIENT CODE
// %%%%%%%%%%%%%%%%%%%%%

WiFiClient client;
// AsyncWebServer staServer(80);

void accessPointConnect() {
    // WiFi.mode(WIFI_STA);
    // WiFi.begin(ssid, pass);           // connects to the WiFi AP

    // Serial.println("\nConnection to the AP ");
    // while (WiFi.status() != WL_CONNECTED) {
    //     Serial.print(".");
    //     delay(500);
    // }

    // Serial.print("LocalIP:"); Serial.println(WiFi.localIP());
    // Serial.println("MAC:" + WiFi.macAddress());
}

void serverPing() {
    // client.connect(serverIP, 80);
    // digitalWrite(2, LOW);
    // Serial.println("********************************");
    // Serial.print("Byte sent to the AP: ");
    // Serial.println(client.print(WiFi.localIP().toString() + "\r"));
    
    // String answer = client.readStringUntil('\r');
    // Serial.println("From the AP: " + answer);
    // client.flush();
    // digitalWrite(2, HIGH);
    // client.stop();
    // delay(1000);
}


bool isAP = true;

void startTheShow() {
    if (isAP) {
        accessPointStart();
    } else {
        accessPointConnect();
    }
}

void runTheShow() {
    if (isAP) {
        clientPingResponse();
    } else {
        serverPing();
    }
}